//
//  ViewController.swift
//  BannersDetectionVideo
//
//  Created by Денис on 13/11/2018.
//  Copyright © 2018 Денис. All rights reserved.
//

import UIKit
import SceneKit
import ARKit
import Vision
import CoreML

class ViewController: UIViewController {

    @IBOutlet var sceneView: ARSCNView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var confidenceLabel: UILabel!
    @IBOutlet weak var blurEffectButton: UIVisualEffectView!
    
    let bubbleDepth: Float = -0.01 // the 'depth' of 3D text
    var referenceImagesSet = Set<ARReferenceImage>()
    var referenceImagesStack = [ARReferenceImage]()
    var worldCoord = SCNVector3()
    
    lazy var rectanglesRequest: VNDetectRectanglesRequest = {
        let request = VNDetectRectanglesRequest(completionHandler: handleRectangles)
        request.regionOfInterest = CGRect(x: 0.075, y: 0.075, width: 0.85, height: 0.85)
//        request.maximumAspectRatio = 1.33
//        request.minimumAspectRatio = 0.75
        request.minimumConfidence = 0.75
        request.maximumObservations = 5
        return request
    }()
    
    lazy var classificationRequest: VNCoreMLRequest = {
        do {
            let model = try VNCoreMLModel(for: BanersDetection().model)
            return VNCoreMLRequest(model: model, completionHandler: handleClassification)
        } catch {
            fatalError("can't load Vision ML model: \(error)")
        }
    }()
    
    //BORDER OF BLUR VIEW
    func blurEfectScale(blurView: UIVisualEffectView) {
        blurView.layer.borderWidth = 2
        blurView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        blurView.layer.cornerRadius = 15
        blurView.clipsToBounds = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupSceneView()
        blurEfectScale(blurView: blurEffectButton)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        resetTrackingConfiguration()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // Pause the view's session
        sceneView.session.pause()
    }
    
    // MARK: - Actions
    func handleRectangles(request: VNRequest, error: Error?) {
        guard let observations = request.results as? [VNRectangleObservation] else {
            fatalError("unexpected result type from VNDetectRectanglesRequest")
        }
        
        guard !observations.isEmpty else {
            print("Doesn't see any rectangles")
            return
        }
        
        guard let currentFrame = sceneView.session.currentFrame else { return }
        let inputImage = CIImage(cvPixelBuffer: currentFrame.capturedImage)
        
        var obsFirst = observations.first
        //BIGGER RECTANGLE
        for firstObservation in observations {
//            if firstObservation.boundingBox.height * firstObservation.boundingBox.width > (obsFirst?.boundingBox.height)! * (obsFirst?.boundingBox.width)! {
//                obsFirst = firstObservation
//            }
            let imageSize = inputImage.extent.size
            // Verify detected rectangle is valid.
            let boundingBox = firstObservation.boundingBox.scaled(to: imageSize)
            guard inputImage.extent.contains(boundingBox) else {
                print("invalid detected rectangle");
                return
            }
            // Rectify the detected image and reduce it to inverted grayscale for applying model.
            let topLeft = firstObservation.topLeft.scaled(to: imageSize)
            let topRight = firstObservation.topRight.scaled(to: imageSize)
            let bottomLeft = firstObservation.bottomLeft.scaled(to: imageSize)
            let bottomRight = firstObservation.bottomRight.scaled(to: imageSize)
            let correctedImage = inputImage
                .cropped(to: boundingBox)
                .applyingFilter("CIPerspectiveCorrection", parameters: [
                    "inputTopLeft": CIVector(cgPoint: topLeft),
                    "inputTopRight": CIVector(cgPoint: topRight),
                    "inputBottomLeft": CIVector(cgPoint: bottomLeft),
                    "inputBottomRight": CIVector(cgPoint: bottomRight)
                    ])
                .oriented(.right)
            
            guard let cgImage = correctedImage.convertToCGImage() else { return }
            let referenceImage = ARReferenceImage(cgImage, orientation: .up, physicalWidth: 0.2)
            referenceImagesStack.append(referenceImage)
            referenceImagesSet.insert(referenceImage)
            resetTrackingConfiguration()
            
            //POSITION OF RECTANGLE
            let centerOfImage: CGPoint = CGPoint(x: boundingBox.midX, y: boundingBox.midY)
            let arHitTestResults : [ARHitTestResult] = (self.sceneView.hitTest(centerOfImage, types: [.featurePoint]))
            if let closestResult = arHitTestResults.first {
                // Get Coordinates of HitTest
                let transform : matrix_float4x4 = closestResult.worldTransform
                self.worldCoord = SCNVector3Make(transform.columns.3.x, transform.columns.3.y, transform.columns.3.z)
            }
            
            // Show the pre-processed image
            DispatchQueue.main.async { [weak self] in
                self?.imageView.image = UIImage(ciImage: correctedImage)
                self?.imageView.isHidden = false
            }
            
            let handler = VNImageRequestHandler(ciImage: correctedImage)
            do {
                try handler.perform([self.classificationRequest])
            } catch {
                print(error)
            }
        }
    }
    
    func handleClassification(request: VNRequest, error: Error?) {
        guard let observation = request.results as? [VNClassificationObservation]
            else { fatalError("unexpected result type from VNCoreMLRequest") }
        guard let best = observation.first
            else { fatalError("can't get best result") }
        
        guard let referenceImage = referenceImagesStack.popLast() else { return }
        referenceImage.name = best.identifier
        referenceImagesSet.update(with: referenceImage)

        DispatchQueue.main.async { [weak self] in
            self?.confidenceLabel.text = best.identifier
            self?.confidenceLabel.isHidden = false
        }
    }
    
    @objc func tapHandler(sender: UITapGestureRecognizer) {
        
        guard let currentFrame = sceneView.session.currentFrame else { return }

        let handler = VNImageRequestHandler(cvPixelBuffer: currentFrame.capturedImage)
        DispatchQueue.global(qos: .userInteractive).async {
            do {
                try handler.perform([self.rectanglesRequest])
            } catch {
                print(error)
            }
        }
    }
    
    @objc func viewsTapHandler(sender: UITapGestureRecognizer) {
        imageView.isHidden = true
        confidenceLabel.isHidden = true
    }
    
    @IBAction func clearScene(_ sender: Any) {
        referenceImagesSet.removeAll()
        referenceImagesStack.removeAll()
        resetTrackingConfiguration()
    }
    
    private func setupViews() {
        let tapRecongizer = UITapGestureRecognizer(target: self, action: #selector(viewsTapHandler(sender:)))
        confidenceLabel.addGestureRecognizer(tapRecongizer)
        confidenceLabel.isHidden = true
        imageView.addGestureRecognizer(tapRecongizer)
        imageView.isHidden = true
    }
    
    private func setupSceneView() {
        let tapRecongizer = UITapGestureRecognizer(target: self, action: #selector(tapHandler(sender:)))
        sceneView.addGestureRecognizer(tapRecongizer)
        sceneView.delegate = self
    }
    
    private func resetTrackingConfiguration() {
        let configuration = ARWorldTrackingConfiguration()
        configuration.detectionImages = referenceImagesSet
        let options: ARSession.RunOptions = [.resetTracking, .removeExistingAnchors]
        sceneView.session.run(configuration, options: options)
    }
    
    private func resetTrackingLogoDetection() {
        guard let referenceImages = ARReferenceImage.referenceImages(inGroupNamed: "AR Resources", bundle: nil) else { return }
        let configuration = ARWorldTrackingConfiguration()
        configuration.detectionImages = referenceImages
        let options: ARSession.RunOptions = [.resetTracking, .removeExistingAnchors]
        sceneView.session.run(configuration, options: options)
    }
    
    //Configuration with detection of logo our company
    private func resetTrackingReferenceImage() {
        guard let referenceImages = ARReferenceImage.referenceImages(inGroupNamed: "AR Resources", bundle: nil) else { return }
        let configuration = ARWorldTrackingConfiguration()
        configuration.detectionImages = referenceImages
        let options: ARSession.RunOptions = [.resetTracking, .removeExistingAnchors]
        sceneView.session.run(configuration, options: options)
    }
    
    // MARK: - Method
    func session(_ session: ARSession, didFailWithError error: Error) {
        // Present an error message to the user
        
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        // Inform the user that the session has been interrupted, for example, by presenting an overlay
        
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        // Reset tracking and/or remove existing anchors if consistent tracking is required
        
    }
}

// MARK: - CREATE ARKIT OBJECTS
extension ViewController: ARSCNViewDelegate {
    
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        DispatchQueue.main.async {
            guard let imageAnchor = anchor as? ARImageAnchor else { return }
            let planeNode = self.getNodes(withReferenceImage: imageAnchor.referenceImage)
            planeNode.opacity = 1.0
            planeNode.eulerAngles.x = -.pi / 2
            node.addChildNode(planeNode)
            node.position = self.worldCoord
        }
    }
    
    //Get bubble text node
    func getBubbleText(ReferenceImage image: ARReferenceImage) -> SCNNode {
        let bubble = SCNText(string: image.name, extrusionDepth: CGFloat(bubbleDepth))
        var font = UIFont(name: "Futura", size: 0.15)
        font = font?.withTraits(traits: .traitBold)
        bubble.font = font
        bubble.alignmentMode = CATextLayerAlignmentMode.center.rawValue
        bubble.firstMaterial?.diffuse.contents = UIColor.orange
        bubble.firstMaterial?.specular.contents = UIColor.white
        bubble.firstMaterial?.isDoubleSided = true
        bubble.chamferRadius = CGFloat(bubbleDepth)
        // BUBBLE NODE
        let (minBound, maxBound) = bubble.boundingBox
        let bubbleNode = SCNNode(geometry: bubble)
        // Centre Node - to Centre-Bottom point
        bubbleNode.pivot = SCNMatrix4MakeTranslation( (maxBound.x - minBound.x)/2, minBound.y, bubbleDepth/2)
        // Reduce default text size
        bubbleNode.scale = SCNVector3Make(0.2, 0.2, 0.2)
        return bubbleNode
    }
    
    //Get sphere node
    func getSphereNode() -> SCNNode {
        let sphere = SCNSphere(radius: 0.0015)
        sphere.firstMaterial?.diffuse.contents = UIColor.cyan
        let sphereNode = SCNNode(geometry: sphere)
        return sphereNode
    }
    
    func getUrlFromImage(imageName: String) -> URL? {
        var url: String = ""
        let lowercaseImageName = imageName.lowercased().trimmingCharacters(in: .whitespaces)
        switch lowercaseImageName {
        case "beeline":
            url = "https://www.beeline.ru"
        case "mtc":
            url = "https://www.mtc.ru"
        case "tele2":
            url = "https://www.tele2.ru"
        case "megafon":
            url = "https://www.megafon.ru"
        default:
            url = "https://www.yandex.ru"
        }
        let urlFromString = URL(string: url)
        return urlFromString
    }
    
    func getWebView(url: URL, with ratio: CGFloat) -> UIWebView {
        let height: CGFloat = 480
        let width = height * ratio
        let webView = UIWebView(frame: CGRect(x: 0, y: 0, width: width, height: height))
        let request = URLRequest(url: url)
        
        DispatchQueue.main.async {
            webView.loadRequest(request)
        }
        return webView
    }
    
    //Get 3D objects from ARKit
    func getNodes(withReferenceImage image: ARReferenceImage) -> SCNNode {
        let bubbleText = getBubbleText(ReferenceImage: image)
        let sphereNode = getSphereNode()
        // BUBBLE PARENT NODE
        let bubbleNodeParent = SCNNode()
        bubbleNodeParent.addChildNode(bubbleText)
        bubbleNodeParent.addChildNode(sphereNode)
        
        let plane = SCNPlane(width: image.physicalSize.width, height: image.physicalSize.height)
        let node = SCNNode(geometry: plane)
        
        if let imageName = image.name, let url = getUrlFromImage(imageName: imageName) {
            let ratio = plane.width / plane.height
            let webView = getWebView(url: url, with: ratio)
            
            plane.firstMaterial?.diffuse.contents = webView
            plane.firstMaterial?.isDoubleSided = true
            bubbleNodeParent.addChildNode(node)
        }
        return bubbleNodeParent
    }
}

// MARK: - EXTENSION FOR UIFONT
extension UIFont {
    func withTraits(traits:UIFontDescriptor.SymbolicTraits...) -> UIFont {
        let descriptor = self.fontDescriptor.withSymbolicTraits(UIFontDescriptor.SymbolicTraits(traits))
        return UIFont(descriptor: descriptor!, size: 0)
    }
}
